# typescript-express-mysql

Nesse boilerplate vou seguir os seguintes itens:

## Checklist

- [ ] [Typescript]
- [ ] [Models]
- [ ] [ExpressJS]
- [ ] [Migrations (MYSQL)]
- [ ] [Environment]

## Comandos disponiveis

- [ ] [npm run dev] => Inicia com Nodemon
- [ ] [npm run build] => Compila e envia para pasta dist
